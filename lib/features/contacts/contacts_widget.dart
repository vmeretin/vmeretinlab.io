import 'package:cv/core/localization/app_localizations.dart';
import 'package:cv/core/ui/constants/app_urls.dart';
import 'package:cv/core/ui/widgets/button/app_circle_button.dart';
import 'package:cv/platform/url_launcher/url_launcher_service.dart';
import 'package:flutter/widgets.dart';

class ContactsWidget extends StatelessWidget {
  const ContactsWidget();

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          Text(
            AppLocalizations.of(context).contactsTitle,
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AppCircleButton(
                svgAssetPath: 'assets/icons/email.svg',
                onPressed: () => UrlLauncherService.openLink(AppUrls.emailUrl),
              ),
              const SizedBox(width: 16),
              AppCircleButton(
                svgAssetPath: 'assets/icons/github_logo.svg',
                onPressed: () => UrlLauncherService.openLink(AppUrls.githubUrl),
              ),
              const SizedBox(width: 16),
              AppCircleButton(
                svgAssetPath: 'assets/icons/telegram_logo.svg',
                onPressed: () => UrlLauncherService.openLink(AppUrls.telegramUrl),
              ),
            ],
          ),
        ],
      );
}

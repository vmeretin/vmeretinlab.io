import 'package:cv/core/localization/app_localizations.dart';
import 'package:cv/core/ui/constants/app_colors.dart';
import 'package:cv/core/ui/constants/app_urls.dart';
import 'package:cv/platform/url_launcher/url_launcher_service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class FooterWidget extends StatelessWidget {
  const FooterWidget();

  @override
  Widget build(BuildContext context) => ColoredBox(
        color: Theme.of(context).colorScheme.inverseSurface,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 32,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/images/flutter_logo.png',
                height: 48,
              ),
              const SizedBox(height: 16),
              Text.rich(
                TextSpan(
                  text: AppLocalizations.of(context).footerSiteMadeOn1,
                  style: TextStyle(
                    fontFamily: 'JetBrainsMono',
                    color: Theme.of(context).colorScheme.onInverseSurface,
                  ),
                  children: [
                    TextSpan(
                      text: AppLocalizations.of(context).footerSiteMadeOn2,
                      style: const TextStyle(
                        color: AppColors.link,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => UrlLauncherService.openLink(AppUrls.flutterWebUrl),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 16),
              Text.rich(
                TextSpan(
                  text: AppLocalizations.of(context).footerSourceCode1,
                  style: TextStyle(
                    fontFamily: 'JetBrainsMono',
                    color: Theme.of(context).colorScheme.onInverseSurface,
                  ),
                  children: [
                    TextSpan(
                      text: AppLocalizations.of(context).footerSourceCode2,
                      style: const TextStyle(
                        color: AppColors.link,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => UrlLauncherService.openLink(AppUrls.sourceCodeUrl),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      );
}

import 'package:cv/core/app/app_cubit.dart';
import 'package:cv/core/localization/app_locale_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LocaleButton extends StatelessWidget {
  const LocaleButton();

  @override
  Widget build(BuildContext context) => IconButton(
        onPressed: () => context.read<AppCubit>().onLocalePressed(),
        icon: Text(
          context.locale.name.toUpperCase(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            letterSpacing: 1.5,
            fontSize: 20,
            color: Theme.of(context).colorScheme.onInverseSurface,
          ),
        ),
        splashRadius: 24,
      );
}

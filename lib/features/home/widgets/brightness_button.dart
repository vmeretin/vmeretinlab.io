import 'package:cv/core/app/app_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BrightnessButton extends StatelessWidget {
  const BrightnessButton();

  @override
  Widget build(BuildContext context) => IconButton(
        onPressed: () => context.read<AppCubit>().onBrightnessPressed(
              currentBrightness: Theme.of(context).brightness,
            ),
        icon: SvgPicture.asset(
          'assets/icons/brightness.svg',
          colorFilter: ColorFilter.mode(
            Theme.of(context).colorScheme.onInverseSurface,
            BlendMode.srcIn,
          ),
        ),
        splashRadius: 24,
      );
}

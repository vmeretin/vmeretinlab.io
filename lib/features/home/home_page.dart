import 'package:cv/core/app/app_cubit.dart';
import 'package:cv/core/localization/app_localizations.dart';
import 'package:cv/core/ui/widgets/adaptive/adaptive_content.dart';
import 'package:cv/features/about_me/about_me_widget.dart';
import 'package:cv/features/contacts/contacts_widget.dart';
import 'package:cv/features/experience/presentation/experience_widget.dart';
import 'package:cv/features/home/widgets/brightness_button.dart';
import 'package:cv/features/home/widgets/footer_widget.dart';
import 'package:cv/features/home/widgets/locale_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scrollController = ScrollController();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    context.read<AppCubit>().onInit(initLocale: Localizations.localeOf(context));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inverseSurface,
          foregroundColor: Theme.of(context).colorScheme.onInverseSurface,
          actions: const [
            SizedBox(width: 16),
            LocaleButton(),
            SizedBox(width: 16),
            BrightnessButton(),
            SizedBox(width: 16),
          ],
        ),
        body: SelectionArea(
          child: Scrollbar(
            controller: _scrollController,
            child: SingleChildScrollView(
              controller: _scrollController,
              physics: const ClampingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 48,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        AdaptiveContent(
                          landscapeFactor: 0.25,
                          portraitFactor: 0.75,
                          child: ClipOval(
                            child: Image.asset('assets/images/avatar.jpg'),
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          AppLocalizations.of(context).fullName,
                          style: const TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 8),
                        Text(
                          AppLocalizations.of(context).position,
                          style: const TextStyle(
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 48),
                        const AdaptiveContent(
                          child: ExperienceWidget(),
                        ),
                        const SizedBox(height: 48),
                        const AdaptiveContent(
                          child: AboutMeWidget(),
                        ),
                        const SizedBox(height: 48),
                        const ContactsWidget(),
                      ],
                    ),
                  ),
                  const FooterWidget(),
                ],
              ),
            ),
          ),
        ),
      );
}

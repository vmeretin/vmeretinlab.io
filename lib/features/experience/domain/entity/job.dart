class Job {
  final String companyName;
  final String companyUrl;
  final DateTime dateStart;
  final DateTime? dateFinish;
  final String? positionEn;
  final String? positionRu;
  final String experienceMarkdownEn;
  final String experienceMarkdownRu;

  Job({
    required this.companyName,
    required this.companyUrl,
    required this.dateStart,
    required this.dateFinish,
    required this.positionEn,
    required this.positionRu,
    required this.experienceMarkdownEn,
    required this.experienceMarkdownRu,
  });
}

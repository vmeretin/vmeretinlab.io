// ignore_for_file: avoid-non-ascii-symbols

import 'package:cv/features/experience/domain/entity/job.dart';

abstract class JobsStorage {
  static final codderz = Job(
    companyName: 'Codderz',
    companyUrl: 'https://codderz.com/',
    dateStart: DateTime(2017, 6),
    dateFinish: DateTime(2018, 2),
    positionEn: 'Intern, Junior Android developer',
    positionRu: 'Стажёр, Junior Android разработчик',
    experienceMarkdownEn: '''
Learnt the basics: OOP, SOLID, Dependency Injection, Clean Architecture, MVP, Kotlin, RxJava.

Developed the following projects:
- Meditation app (MediaPlayer, InAppBilling, offline, sync with backend)
- Photo editor (android.graphics, TextureView, Device administration API, Camera) 
- Framework for new projects (Multi-module project, Android libraries development)
- Time tracking app
''',
    experienceMarkdownRu: '''
Изучал основы: ООП, SOLID, Dependency Injection, Clean Architecture, MVP, Kotlin, RxJava.

Разрабатывал следующие проекты:
- Приложение для медитации (MediaPlayer, InAppBilling, работа в оффлайн, синхронизация с сервером)
- Редактор фотографий (android.graphics, TextureView, Device administration API, Camera) 
- Фреймворк для новых проектов (Multi-module project, разработка Android-библиотек)
- Приложение для учета времени
''',
  );

  static final bazilik = Job(
    companyName: 'Bazilik',
    companyUrl: 'https://gitlab.com/bazilik',
    dateStart: DateTime(2018, 2),
    dateFinish: DateTime(2018, 7),
    positionEn: null,
    positionRu: null,
    experienceMarkdownEn: '''
Graduate work in Astrakhan State University.        
        
Food delivery service like Delivery Club with integration of social networks elements.

Included 3 applications:
- [Android](https://gitlab.com/bazilik/bazilik-android) (MVVM, Koin, Jetpack, Firebase Auth, Geolocation, VK-SDK)
- [Backend](https://gitlab.com/bazilik/bazilik-api) (Kotlin, Spring Boot, PostgreSQL, JWT, Firebase Admin)
- [Frontend](https://gitlab.com/bazilik/bazilik-frontend) (VueJS, Bootstrap, Axios)
''',
    experienceMarkdownRu: '''
Дипломная работа в Астраханском Государственном Университете.
        
Сервис доставки еды по типу Delivery Club с интеграцией элементов соцсети.

Включал в себя 3 приложения:
- [Android](https://gitlab.com/bazilik/bazilik-android) (MVVM, Koin, Jetpack, Firebase Auth, Geolocation, VK-SDK)
- [Backend](https://gitlab.com/bazilik/bazilik-api) (Kotlin, Spring Boot, PostgreSQL, JWT, Firebase Admin)
- [Frontend](https://gitlab.com/bazilik/bazilik-frontend) (VueJS, Bootstrap, Axios)
''',
  );

  static final baccaSoft = Job(
    companyName: 'BaccaSoft',
    companyUrl: 'https://baccasoft.com/',
    dateStart: DateTime(2018, 9),
    dateFinish: DateTime(2019, 9),
    positionEn: 'Middle Android developer',
    positionRu: 'Middle Android разработчик',
    experienceMarkdownEn: '''
During the probation I supported old projects with a lot of legacy code.

After the probation, I participated in development of two major projects:
1) b2c fuel payment service at gas stations
    - completion of the first version of the app (changed the payment logic, bug fix)
    - redesign of the app (made new widgets, changed navigation from NavigationDrawer to BottomNavBar, migrated from Cicerone 3 to Cicerone 5)
    - new features were added
2) b2e mobile workplace for bank managers

    Development from scratch to release with subsequent support and the addition of new functionality.
    
    The main functionality of the app:
    - a large number of forms with complex validation
    - printing documents received from the backend (android.print)
    - camera for photographing completed documents
    - photo compression
    - sync with the backend
    - a large number of types of push notifications with complex navigation in the app

In fact, on these two projects I was a lead developer in a team with two or three developers:
built the architecture, distributed tasks, made code reviews.
''',
    experienceMarkdownRu: '''
На испытательном сроке занимался поддержкой старых проектов с большим количеством legacy кода.

После испытательного срока участвовал в разработке двух крупных проектах:
1) b2c сервис оплаты топлива на заправках
    - доработка первой версии приложения (меняли логику оплаты, правили баги)
    - редизайн приложения (делали новые виджеты, меняли навигацию с NavigationDrawer на BottomNavBar, мигрировали с Cicerone 3 на Cicerone 5)
    - добавляли новый функционал
2) b2e мобильное рабочее место для менеджеров банка

    Разработка с нуля до релиза с последующей поддержкой и добавлением нового функционала.
    
    Основной функционал приложения: 
    - большое количество форм со сложной валидацией 
    - печать документов, полученных с сервера (android.print)
    - камера для фотографирования заполненных документов
    - сжатие фотографий
    - синхронизация с сервером
    - большое количество типов push-уведомлений со сложной навигацией в приложении

На этих двух проектах по факту был ведущим разработчиком в команде с двумя-тремя коллегами: 
выстраивал архитектуру, распределял задачи, проводил код-ревью.
''',
  );

  static final storeMood = Job(
    companyName: 'StoreMood',
    companyUrl: 'https://storemood.com/',
    dateStart: DateTime(2019, 9),
    dateFinish: DateTime(2020),
    positionEn: 'Flutter developer',
    positionRu: 'Flutter разработчик',
    experienceMarkdownEn: '''
Development of 2 mobile applications:
- Application for scanning QR codes
- Application for photographers:
  - interaction with a DSLR-camera via USB-OTG cable
  - photo compression
  - horizontal layout of screens (the mobile phone is attached to the camera on a tripod)

Both applications communicate with the server through gRPC.
''',
    experienceMarkdownRu: '''
Разработка 2 мобильных приложений:
- Приложение для сканирования QR-кодов
- Приложение для фотографов:
  - взаимодействие с DSLR-камерой через USB-OTG кабель
  - сжатие фотографий
  - горизонтальная вёрстка экранов (телефон прикрепляется к камере на штатив)

Оба приложения взаимодействуют с сервером через gRPC.
''',
  );

  static final speakPal = Job(
    companyName: 'Speak Pal',
    companyUrl: 'https://speakpal.club/',
    dateStart: DateTime(2020, 3),
    dateFinish: DateTime(2024, 11),
    positionEn: 'Lead mobile developer',
    positionRu: 'Ведущий мобильный разработчик',
    experienceMarkdownEn: '''
- Development and support of a mobile application on Flutter
- Development of cross-platform Flutter plugins
- Setting up and maintaining a CI/CD server
- Building the project architecture
- Code review
    ''',
    experienceMarkdownRu: '''
- Разработка и поддержка мобильного приложения на Flutter
- Разработка кроссплатформенных Flutter-плагинов
- Настройка и поддержка CI/CD-сервера
- Построение архитектуры проекта
- Code review
    ''',
  );

  static final leClick = Job(
    companyName: 'LeClick',
    companyUrl: 'https://leclick.ru/',
    dateStart: DateTime(2023),
    dateFinish: DateTime(2024, 12),
    positionEn: 'Flutter developer',
    positionRu: 'Flutter разработчик',
    experienceMarkdownEn: '''
- Building the project architecture
- Development and support of a mobile application on Flutter
- Setting up and maintaining a CI/CD server
    ''',
    experienceMarkdownRu: '''
- Построение архитектуры проекта
- Разработка и поддержка мобильного приложения на Flutter
- Настройка и поддержка CI/CD-сервера
    ''',
  );
}

import 'package:cv/core/localization/app_locale.dart';
import 'package:cv/core/localization/app_locale_extensions.dart';
import 'package:cv/core/ui/constants/app_colors.dart';
import 'package:cv/core/ui/widgets/text/markdown_text.dart';
import 'package:cv/features/experience/domain/entity/job.dart';
import 'package:cv/platform/url_launcher/url_launcher_service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class JobWidget extends StatelessWidget {
  final Job job;

  const JobWidget({
    required this.job,
  });

  @override
  Widget build(BuildContext context) => Card(
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  text: job.companyName,
                  style: const TextStyle(
                    fontFamily: 'JetBrainsMono',
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                    color: AppColors.link,
                  ),
                  recognizer: TapGestureRecognizer()..onTap = () => UrlLauncherService.openLink(job.companyUrl),
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 8),
              _DateWidget(
                dateStart: job.dateStart,
                dateFinish: job.dateFinish,
              ),
              _PositionWidget(
                position: switch (context.locale) {
                  AppLocale.en => job.positionEn,
                  AppLocale.ru => job.positionRu,
                },
              ),
              const SizedBox(height: 16),
              MarkdownText(
                text: switch (context.locale) {
                  AppLocale.en => job.experienceMarkdownEn,
                  AppLocale.ru => job.experienceMarkdownRu,
                },
              ),
            ],
          ),
        ),
      );
}

class _DateWidget extends StatelessWidget {
  final DateTime dateStart;
  final DateTime? dateFinish;

  DateFormat get _dateFormat => DateFormat('MM.yyyy');

  const _DateWidget({
    required this.dateStart,
    required this.dateFinish,
  });

  @override
  Widget build(BuildContext context) {
    final dateStartString = _dateFormat.format(dateStart);
    final dateFinish = this.dateFinish;
    // Infinity sign
    // ignore: avoid-non-ascii-symbols
    final dateFinishString = dateFinish != null ? _dateFormat.format(dateFinish) : '∞';

    return Text(
      '($dateStartString - $dateFinishString)',
      style: const TextStyle(
        fontSize: 16,
      ),
      textAlign: TextAlign.center,
    );
  }
}

class _PositionWidget extends StatelessWidget {
  final String? position;

  const _PositionWidget({
    required this.position,
  });

  @override
  Widget build(BuildContext context) => Visibility(
        visible: position != null,
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Text(
            position ?? '',
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      );
}

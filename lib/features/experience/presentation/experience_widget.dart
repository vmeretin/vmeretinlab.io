import 'dart:async';

import 'package:cv/core/localization/app_localizations.dart';
import 'package:cv/core/utils/iterators_helper.dart';
import 'package:cv/features/experience/presentation/experience_cubit.dart';
import 'package:cv/features/experience/presentation/widget/job_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ExperienceWidget extends StatefulWidget {
  const ExperienceWidget();

  @override
  _ExperienceWidgetState createState() => _ExperienceWidgetState();
}

class _ExperienceWidgetState extends State<ExperienceWidget> {
  final _experienceCubit = ExperienceCubit();

  @override
  void initState() {
    super.initState();
    _experienceCubit.onInit();
  }

  @override
  void dispose() {
    unawaited(_experienceCubit.close());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => BlocProvider.value(
        value: _experienceCubit,
        child: BlocBuilder<ExperienceCubit, ExperienceState>(
          builder: (context, state) => Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).experienceTitle,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 8),
                  IconButton(
                    onPressed: () => context.read<ExperienceCubit>().onReversePressed(),
                    icon: SvgPicture.asset(
                      'assets/icons/swap_vert.svg',
                      width: 24,
                      height: 24,
                      colorFilter: ColorFilter.mode(
                        Theme.of(context).colorScheme.onSurface,
                        BlendMode.srcIn,
                      ),
                    ),
                    splashRadius: 24,
                  ),
                ],
              ),
              const SizedBox(height: 16),
              Column(
                children: intersperse(
                  const SizedBox(height: 32),
                  state.jobList.map((job) => JobWidget(job: job)),
                ).toList(),
              ),
            ],
          ),
        ),
      );
}

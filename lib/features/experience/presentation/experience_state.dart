part of 'experience_cubit.dart';

@immutable
class ExperienceState {
  final List<Job> jobList;
  final bool reversed;

  const ExperienceState({
    required this.jobList,
    required this.reversed,
  });

  factory ExperienceState.initial() => const ExperienceState(
        jobList: [],
        reversed: true,
      );

  ExperienceState copyWith({
    List<Job>? jobList,
    bool? reversed,
  }) =>
      ExperienceState(
        jobList: jobList ?? this.jobList,
        reversed: reversed ?? this.reversed,
      );
}

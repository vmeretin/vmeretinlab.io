import 'package:cv/features/experience/data/jobs_storage.dart';
import 'package:cv/features/experience/domain/entity/job.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'experience_state.dart';

class ExperienceCubit extends Cubit<ExperienceState> {
  ExperienceCubit() : super(ExperienceState.initial());

  void onInit() => emit(state.copyWith(
        jobList: [
          JobsStorage.leClick,
          JobsStorage.speakPal,
          JobsStorage.storeMood,
          JobsStorage.baccaSoft,
          JobsStorage.bazilik,
          JobsStorage.codderz,
        ],
      ));

  void onReversePressed() => emit(state.copyWith(
        reversed: state.reversed == false,
        jobList: state.jobList.reversed.toList(),
      ));
}

import 'package:cv/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

class AboutMeWidget extends StatelessWidget {
  const AboutMeWidget();

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          Text(
            AppLocalizations.of(context).aboutMeTitle,
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 16),
          Card(
            elevation: 4,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                AppLocalizations.of(context).aboutMeContent,
              ),
            ),
          ),
        ],
      );
}

import 'dart:async';

import 'package:url_launcher/url_launcher.dart';

abstract class UrlLauncherService {
  static void openLink(String url) => unawaited(launchUrl(Uri.parse(url)));
}

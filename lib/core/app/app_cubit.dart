import 'package:cv/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(AppState.initial());

  void onInit({
    required Locale initLocale,
  }) =>
      emit(state.copyWith(
        locale: initLocale,
      ));

  void onLocalePressed() {
    final currentLocale = state.locale;
    if (currentLocale == null) {
      return;
    }

    const supportedLocales = AppLocalizations.supportedLocales;
    final currentIndex = supportedLocales.indexOf(currentLocale);

    emit(state.copyWith(
      locale: currentIndex + 1 == supportedLocales.length ? supportedLocales.first : supportedLocales[currentIndex + 1],
    ));
  }

  void onBrightnessPressed({
    required Brightness currentBrightness,
  }) {
    final nextBrightness = _invertBrightness(currentBrightness);

    emit(state.copyWith(
      themeMode: _mapThemeMode(nextBrightness),
    ));
  }

  Brightness _invertBrightness(Brightness brightness) =>
      brightness == Brightness.light ? Brightness.dark : Brightness.light;

  ThemeMode _mapThemeMode(Brightness brightness) => switch (brightness) {
        Brightness.dark => ThemeMode.dark,
        Brightness.light => ThemeMode.light,
      };
}

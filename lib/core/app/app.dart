import 'package:cv/core/app/app_cubit.dart';
import 'package:cv/core/localization/app_localizations.dart';
import 'package:cv/core/ui/constants/app_themes.dart';
import 'package:cv/features/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocProvider<AppCubit>(
        create: (context) => AppCubit(),
        child: BlocBuilder<AppCubit, AppState>(
          builder: (context, state) => MaterialApp(
            home: HomePage(),
            theme: AppThemes.get(
              brightness: Brightness.light,
            ),
            darkTheme: AppThemes.get(
              brightness: Brightness.dark,
            ),
            themeMode: state.themeMode,
            locale: state.locale,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            onGenerateTitle: (context) => AppLocalizations.of(context).appTitle,
            debugShowCheckedModeBanner: false,
          ),
        ),
      );
}

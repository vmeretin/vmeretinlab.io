part of 'app_cubit.dart';

@immutable
class AppState {
  final Locale? locale;
  final ThemeMode themeMode;

  const AppState({
    required this.locale,
    required this.themeMode,
  });

  factory AppState.initial() => const AppState(
        locale: null,
        themeMode: ThemeMode.system,
      );

  AppState copyWith({
    Locale? locale,
    ThemeMode? themeMode,
  }) =>
      AppState(
        locale: locale ?? this.locale,
        themeMode: themeMode ?? this.themeMode,
      );
}

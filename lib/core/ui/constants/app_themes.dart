import 'package:flutter/material.dart';

abstract class AppThemes {
  static ThemeData get({
    required Brightness brightness,
  }) =>
      ThemeData(
        fontFamily: 'JetBrainsMono',
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.indigo,
          brightness: brightness,
        ),
      );
}

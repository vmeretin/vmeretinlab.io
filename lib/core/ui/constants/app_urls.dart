abstract class AppUrls {
  static const emailUrl = 'mailto:keshmannn@gmail.com';
  static const githubUrl = 'https://github.com/vmeretin';
  static const telegramUrl = 'https://t.me/vmeretin';
  static const flutterWebUrl = 'https://flutter.dev/multi-platform/web';
  static const sourceCodeUrl = 'https://gitlab.com/vmeretin/vmeretin.gitlab.io';
}

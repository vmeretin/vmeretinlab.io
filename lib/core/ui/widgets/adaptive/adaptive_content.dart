import 'package:flutter/widgets.dart';

class AdaptiveContent extends StatelessWidget {
  final double landscapeFactor;
  final double portraitFactor;
  final Widget child;

  const AdaptiveContent({
    required this.child,
    this.landscapeFactor = 0.5,
    this.portraitFactor = 1,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return FractionallySizedBox(
      widthFactor: size.width > size.height ? landscapeFactor : portraitFactor,
      child: child,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppCircleButton extends StatelessWidget {
  final String svgAssetPath;
  final VoidCallback onPressed;

  const AppCircleButton({
    required this.svgAssetPath,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          shape: const CircleBorder(),
          padding: const EdgeInsets.all(12),
          backgroundColor: Theme.of(context).colorScheme.inverseSurface,
          minimumSize: const Size.square(52),
        ),
        child: SvgPicture.asset(
          svgAssetPath,
          colorFilter: ColorFilter.mode(
            Theme.of(context).colorScheme.onInverseSurface,
            BlendMode.srcIn,
          ),
        ),
      );
}

import 'package:cv/core/ui/constants/app_colors.dart';
import 'package:cv/platform/url_launcher/url_launcher_service.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class MarkdownText extends StatelessWidget {
  final String text;

  const MarkdownText({
    required this.text,
  });

  @override
  Widget build(BuildContext context) => MarkdownBody(
        data: text,
        styleSheet: MarkdownStyleSheet(
          a: const TextStyle(
            color: AppColors.link,
          ),
        ),
        onTapLink: (text, href, title) => href != null ? UrlLauncherService.openLink(href) : null,
      );
}

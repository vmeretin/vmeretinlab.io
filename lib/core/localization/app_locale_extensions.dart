import 'package:cv/core/localization/app_locale.dart';
import 'package:flutter/widgets.dart';

extension AppLocaleExtensions on BuildContext {
  AppLocale get locale => AppLocale.values.byName(Localizations.localeOf(this).languageCode);
}

# Curriculum Vitae

- `flutter create --project-name cv --platforms web --overwrite .`
- `dart format -l 120 lib`
- `dart run dependency_validator`
- `dart run flutter_launcher_icons`
